---
layout: handbook-page-toc
title: "Enterprise Sales Playbook"
description: "The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Overview 
The GitLab Enterprise Sales Playbook offers actionable and prescriptive guidance throughout the customer lifecycle to drive repeatable and consistent sales performance. For large strategic accounts, effective [Account Planning](/handbook/sales/account-planning/) (for both Land and Expand accounts) is also key to success.

| Educate & Engage<br>(Sales Stages 0-3) | Facilitate the Opportunity<br>(Sales Stages 3-4) | Deal Closure<br>(Sales Stages 5-6) | Retain and Expand<br>(Growth) |
| ------ | ------ | ------ | ------ |
| * [Prospecting](/handbook/sales/prospecting/)<br> * [Discovery](/handbook/sales/playbook/discovery/)<br> * Articulate Value | * Position to Win<br> * Technical Evaluation<br> * Customer-Centric Proposal | * [Mutual Close Plan](/handbook/sales/mutual-close-plan)<br> * [Negotiate to Close](/handbook/sales/negotiate-to-close/)<br> * [Order Processing](/handbook/sales/field-operations/order-processing/) | * [Account Transition](/handbook/customer-success/pre-sales-post-sales-transition/)<br> * [Customer Onboarding](/handbook/customer-success/tam/onboarding/) <br> * [Success Plans](/handbook/customer-success/tam/success-plans/) <br> * [Executive Business Reviews](/handbook/customer-success/tam/ebr/)<br> * [Customer Health Assessment](/handbook/customer-success/tam/health-score-triage/)<br> * [Renewals](/handbook/customer-success/tam/renewals/) |

_Note: A Commercial Sales Playbook will be developed as a component of the [Commercial Sales Handbook](/handbook/sales/commercial/) (timing TBD)._
