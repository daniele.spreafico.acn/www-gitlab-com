title: Te Herenga Waka—Victoria University of Wellington
file_name: victoria_university
canonical_path: /customers/victoria_university/
cover_image: /images/blogimages/vic_uni_cover_image.jpg
cover_title: |
  GitLab advances open science education at Te Herenga Waka – Victoria University of Wellington
cover_description: |
  GitLab enables software coursework and project management with a single, simplified solution for faculty and students.
twitter_image: /images/blogimages/vic_uni_cover_image.jpg
twitter_text: Learn how GitLab advances open science education at @WellingtonUni
customer_logo: /images/case_study_logos/victoria_university_of_wellington_logo_vector.png
customer_logo_css_class: brand-logo-tall
customer_industry: Education
customer_location: Wellington, New Zealand
customer_solution: GitLab Self Managed Ultimate
customer_employees: |
  2300
customer_overview: |
  The School of Engineering and Computer Science dog foods its curriculum, using GitLab for faculty operations and open science education.
customer_challenge: |
  The administrative tasks of maintaining a multi-toolchain took far too much time and attention. They needed a single DevOps platform for collaboration and coursework. 
key_benefits: >-

  
    Supports IP-compliant server requirements   

  
    Streamlines faculty operations and research

  
    Facilitates hands-on agile coursework

  
    Enables end-to-end assignment visibility 

  
    Provides a single solution for teachers and students


    Offers version control and a single source of truth
customer_stats:
  - stat: 35 courses
    label: 35 GitLab-enabled courses
  - stat: 8,000+  
    label: 8,000+ projects across 2,212 groups
  - stat: 768+
    label: Over 768 active users 
customer_study_content:
  - title: the customer
    subtitle: School of Engineering and Computer Science at New Zealand’s top-ranked research university
    content: >-
  
       Te Herenga Waka – Victoria University of Wellington’s School of Engineering and Computer Science has been New Zealand’s innovation hub since the advent of the country’s first internet connection by Professor John Hine. The greater university, founded in 1897, is the island nation’s leading institution for research quality and intensity.  In this tradition, the School, located within the University’s Faculty of Engineering, developed an expert-led curriculum with a focus on applied learning. The School’s Bachelor of Engineering (with Honors) degree holistically prepares students for real-world technology systems by offering career-minded coursework in project management and entrepreneurship. 


  - title: the challenge
    subtitle:  Finding a single, compliant solution for all user groups 
    content: >-
    

        The School of Engineering and Computer Science’s comprehensive professional program had multiple systems for tracking contributions and content. Lecturers were using Blackboard, the school’s customized Wiki page, personal web pages and even Google Docs for course content. Students were typically given discretion to choose the system to use for their course work. “When I joined the School, my first job was to develop and deliver courses in engineering project management and professional practice. I quickly came to realise that the use of multiple was problematic because few of them provided the features required and there was a lot of unnecessary variability, which complicated both teaching and learning.” says Dr. James Quilty, Program Director for Engineering. 


        Other staff agree. “My experience at another institution running the full Atlassian Stack was that the administration overhead was too high, and the tools were overkill for the size and scope of the work we were doing,” says Dr. Simon McCallum, Senior Lecturer. “I prefer to use professional project management tools, rather than tools that enable less transfer to the real environment.”Few of the previous tools provided proper version history or issue tracking, so collaboration wasn’t possible. Both students and lecturers lacked transparent insights into academic progress. Distributing information via PDFs and Word documents offered little benefit. “Information was fragmented across multiple files, multiple formats – sitting often on file systems, not necessarily under good version control,” says Dr. Quilty.


        The previous environment’s biggest negatives included time lost on administration and the impact that had on the overall education of the students. “Time is the most precious commodity while teaching. The maximum percentage of time should be spent on things that affect student achievement. Admin tasks for the sake of it are wasteful and limit the capacity to create excellent learning environments,” McCallum said. To improve continuity in education and innovation, the engineering program needed a single, integrated solution to simplify processes and encourage adoption across all user groups. However, the university has strict intellectual property and confidentiality agreements which narrowed the School’s platform options.  "I see GitLab as a critical source in open science. The ability to share code that has been created with an open license is great, and I appreciate that most people are nudged to open their projects by providing free hosting for open projects. This encourages more code and resources to be available for universities" McCallum added.

  - title: the solution
    subtitle:  GitLab Self-Managed for on-premise hosting 
    content: >-


        Dr. Quilty acquainted himself with GitLab as a project management tool beginning in 2015. “My current role grew from the project management side of things, and it's in that context of teaching engineering project management in a school that's heavily focused on software engineering in particular, as well as electronics engineering, that I came into contact with GitLab as a product,” Dr. Quilty said.  Previously, educators had remained solution-agnostic – using GitHub, Atlassian, Trello, Subversion, Blackboard, and Fronter. Many academics, including Dr. Quilty, allowed students to choose their own developer tools for project execution. Dr. Quilty soon understood that students often lacked the experience to identify appropriate software solutions, which led to disparate learning experiences. Dr. Quilty briefly explored GitHub as an alternative but preferred GitLab’s open DevOps platform. 

      
        As an organizational experiment, a small group of academics began using GitLab in 2017. Dr. Quilty noted, “I think everybody was using GitLab from that point on. It was a very rapid transition from 2018 onwards for course delivery right through from second year all the way through third to fourth year, as well as some people adopting it for their research.” The University ultimately chose GitLab for the Educational license for the professional suite of tools and onsite hosting. The program seamlessly implemented a single sign-on for GitLab and Mattermost (an open source, university-compliant chat service) to increase collaboration without complicating its toolchain. GitLab LDAP integration further simplified adoption. 

        
        For Dr. Quilty and the greater university, the open platform also served to reinforce academic values. Open source support was a key factor in aligning software with curriculum; other previously considered solutions, such as Microsoft Project, failed in this category. GitLab’s openness extended to project methodologies as well, allowing academics the freedom to teach agile planning without limiting the potential for future courses on other frameworks.

  - title: the results
    subtitle:  Empowering learning and open science education 
    content: >-

        Victoria University of Wellington’s engineering and computer science students now leverage GitLab for managing software projects. Both students and educators have gained transparency by tracking coursework with issues, boards, epics, and milestones – all hosted on a local instance of GitLab per university governance. GitLab is being used both as a tool to manage the content of the class and as a tool for students to use to develop their own content. Students can submit repositories as the submission for assignments. They can then check histories and better manage work with issues and boards.


        “I use it to manage the progress of students through the Master's program,” McCallum said. “Each student is an issue in the service desk, this allows me to share any communication with them, with the supervisor, and provides a single location for all the communication that is shared with the supervisor.” With greater academic transparency came focused collaboration. Dr. Quilty praised the newly acquired processes resulting from the transformation: “When it came to reviewing the students' work, particularly the reports that they had to generate or other documentation, they raised merge requests for the various chapters and then asked for me to go through and do approval. And since all of that was either Markdown or LaTeX, it was really quite good to be able to make comments, suggestions on the original code, have those all in the version control, and multiple connections. It was a really good way of managing that writing process, surprisingly so.”


        The School of Engineering and Computer Science has seen rapid adoption of the platform. As of 2021, student users are up by more than 483% since 2017. The program has also added 34 GitLab-facilitated courses in that same timeframe. Over 2,000 groups at every level, from researchers and lecturers to undergraduates and postgraduates, contribute to more than 8,000 projects.  GitLab has enabled a form of continuous learning at the School of Engineering and Computer Science. Students actively engage in problem-solving – using the platform to get real-time educational feedback. It also helps students prepare for working in a modern, engineering-oriented team environment


        Undergraduate teams working on electronics development have used GitLab to run automated tests of their designs by configuring their .gitlab-ci.yaml script to create a Docker container. The script runs the KiCad electronic design automation (EDA) suite in graphical user interface (GUI) mode to execute these tests. Postgraduate students have employed GitLab to set up hardware-in-the-loop testing with GitLab Runner. By copying and executing the embedded code from the repository to a microcontroller-based device under test, via USB-connection from a small Linux server executing GitLab Runner, the students ran automated tests and success/failure reports to streamline hardware and software co-development.

        With 30,000-plus issues and nearly 15,000 merge requests, it’s easy to see how the engineering program at Victoria University of Wellington has leveraged GitLab-led learning to power innovation. 

   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: When I heard about GitLab Self-Managed, it was a very clear choice. It was really only GitLab that fulfilled the requirements I had within the engineering project management courses. That and GitLab being one single product
    attribution: Dr James Quilty
    attribution_title: Director of Engineering, Te Herenga Waka—Victoria University of Wellington









